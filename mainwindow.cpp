#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QPrintPreviewDialog>
#include <QPrintDialog>
#include <QFileDialog>
#include <QListWidget>
#include <QScrollArea>
#include <QPrinter>
#include <QPainter>
#include <QToolBar>
#include <QLabel>
#include <QPageSetupDialog>
#include <QActionGroup>

#include <QTimer>

//#include <QGenericMatrix>

#include <QTextCursor>
#include <QTextDocument>

#include <QDebug>

loadedImage::loadedImage(QString file_name)
{
    fileName = file_name;
    rotation = 0;
    fitPage = 0;
}

void loadedImage::rotateRight(){
    rotation += 90;
}

void loadedImage::rotateLeft(){
    rotation = rotation - 90;
}

QString loadedImage::getFileName(){
    return fileName;
}

void loadedImage::setFileName(QString file_name){
    fileName = file_name;
}

void loadedImage::setFitPage(int fit_page){
    fitPage = fit_page;
}

int loadedImage::getRotation()
{
    return rotation;
}

int loadedImage::getFitPage()
{
    return fitPage;
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QToolBar * toolBar = new QToolBar;

    addToolBar(Qt::TopToolBarArea, toolBar);


    QAction * open = new QAction (QIcon(":/icons/open-folder.png"), "Open");
    QAction * rotate = new QAction (QIcon(":/icons/rotation_right.png"), "Rotate right");
    QAction * rotate_left = new QAction (QIcon(":/icons/rotation_left.png"), "Rotate left");
    QAction * moveBack = new QAction (QIcon(":/icons/move_up.png"), "Swap with previous");
    QAction * moveForward = new QAction (QIcon(":/icons/move_down.png"), "Swap with next");
    QAction * imageFit = new QAction (QIcon(":/icons/image_fit.png"), "Fit image to page size");
    QAction * printAll = new QAction (QIcon(":/icons/printer.png"), "Print document");
    QAction * firstPage = new QAction (QIcon(":/icons/first_page.png"), "Move to first page");
    QAction * lastPage = new QAction (QIcon(":/icons/last_page.png"), "Move to last page");
    QAction * prevPage = new QAction (QIcon(":/icons/prev_page.png"), "Previous page");
    QAction * nextPage = new QAction (QIcon(":/icons/next_page.png"), "Next page");
    QAction * zoomOut = new QAction (QIcon(":/icons/zoom_out.png"), "Zoom out");
    QAction * zoomIn = new QAction (QIcon(":/icons/zoom_in.png"), "Zoom in");
    QAction * landscapeAct = new QAction (QIcon(":/icons/landscape.png"), "Landscape");
    QAction * portraitAct = new QAction (QIcon(":/icons/portrait.png"), "Portrait");
    QAction * pageSetupAct = new QAction (QIcon(":/icons/page_dialog.png"), "Page setup dialog");
    QAction * singlePage = new QAction (QIcon(":/icons/single_page.png"), "Single page view");
    QAction * facingPage = new QAction (QIcon(":/icons/facing_page.png"), "Facing page view");
    QAction * allPage = new QAction (QIcon(":/icons/all_page.png"), "All page view");
    QAction * fitWidthView = new QAction (QIcon(":/icons/fit_width.png"), "Fit width");
    QAction * fitPageView = new QAction (QIcon(":/icons/fit_page.png"), "Fit page");

    QActionGroup * viewGroup = new QActionGroup(this);
    viewGroup->addAction(singlePage);
    viewGroup->addAction(facingPage);
    viewGroup->addAction(allPage);
    singlePage->setChecked(true);
    singlePage->setCheckable(true);
    facingPage->setCheckable(true);
    allPage->setCheckable(true);

    QActionGroup * zoomGroup = new QActionGroup(this);
    zoomGroup->addAction(fitWidthView);
    zoomGroup->addAction(fitPageView);
    fitPageView->setCheckable(true);
    fitWidthView->setCheckable(true);
    fitPageView->setChecked(true);

    QActionGroup * lpMode = new QActionGroup(this);
    lpMode->addAction(landscapeAct);
    lpMode->addAction(portraitAct);
    landscapeAct->setCheckable(true);
    portraitAct->setCheckable(true);
    portraitAct->setChecked(true);


    imageFit->setCheckable(true);

    toolBar->addAction(open);

    toolBar->addActions(zoomGroup->actions());

    toolBar->addAction(rotate_left);
    toolBar->addAction(rotate);

    toolBar->addAction(firstPage);
    toolBar->addAction(prevPage);
    toolBar->addAction(nextPage);
    toolBar->addAction(lastPage);

    toolBar->addAction(moveBack);
    toolBar->addAction(moveForward);

    toolBar->addAction(zoomOut);
    toolBar->addAction(zoomIn);

    toolBar->addActions(lpMode->actions());

    toolBar->addAction(imageFit);

    toolBar->addActions(viewGroup->actions());
    toolBar->addAction(pageSetupAct);
    toolBar->addAction(printAll);



    printer = new QPrinter();
    previewWidget = new QPrintPreviewWidget (printer, this);

    QLabel imageLabel;

    connect (rotate, SIGNAL(triggered()), this, SLOT(rotate()));
    connect (rotate_left, SIGNAL(triggered()), this, SLOT(rotate_left()));
    connect (open, SIGNAL(triggered()), this, SLOT(on_actionOpen_triggered()));
    connect (moveBack, SIGNAL(triggered()), this, SLOT(moveBackSlot()));
    connect (moveForward, SIGNAL(triggered()), this, SLOT(moveForwardSlot()));
    connect (firstPage, SIGNAL(triggered()), this, SLOT(first_page()));
    connect (lastPage, SIGNAL(triggered()), this, SLOT(last_page()));
    connect (imageFit, SIGNAL(changed()), this, SLOT(image_fit()));
    connect (printAll, SIGNAL(triggered()), this, SLOT(on_actionPrint_triggered()));
    connect (prevPage, SIGNAL(triggered()), this, SLOT(prev_page()));
    connect (nextPage, SIGNAL(triggered()), this, SLOT(next_page()));
    connect (zoomOut, SIGNAL(triggered()), this, SLOT(zoom_out()));
    connect (zoomIn, SIGNAL(triggered()), this, SLOT(zoom_in()));
    connect (landscapeAct, SIGNAL(triggered()), this, SLOT(landscape_slot()));
    connect (portraitAct, SIGNAL(triggered()), this, SLOT(portrait_slot()));
    connect (pageSetupAct, SIGNAL(triggered()), this, SLOT(page_setup_slot()));
    connect (singlePage, SIGNAL(triggered()), this, SLOT(single_page()));
    connect (facingPage, SIGNAL(triggered()), this, SLOT(facing_page()));
    connect (allPage, SIGNAL(triggered()), this, SLOT(all_page()));
    connect (fitWidthView, SIGNAL(triggered()), this, SLOT(fit_width_view()));
    connect (fitPageView, SIGNAL(triggered()), this, SLOT(fit_page_view()));

    QVector <loadedImage> loadedImages;

    QStringList fileNames;


    connect(previewWidget, SIGNAL(paintRequested(QPrinter*)), this, SLOT(print(QPrinter*)));
    setCentralWidget(previewWidget);

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_actionOpen_triggered()
{

    fileNames.append(QFileDialog::getOpenFileNames(this, "Open Image", QDir::currentPath(), "Image Files (*.png *.jpg *.bmp)"));
    for (int i = 0; i < fileNames.size(); ++i)
        {
            loadedImages.append(loadedImage(fileNames.at(i)));
            qDebug() << loadedImages[i].loadedImage::getFileName();
            //obrazky->push_back(QImage(fileNames.at(i)));
        }
    connect(previewWidget, SIGNAL(paintRequested(QPrinter*)), this, SLOT(print(QPrinter*)));
    int soucasna_strana = previewWidget->currentPage();
    qDebug() << "Soucasna stranka: " <<  soucasna_strana;

    previewWidget->updatePreview();


}


void MainWindow::on_actionExit_triggered()
{

}


void MainWindow::on_actionPrint_triggered()
{

    printer->setResolution(300);
    printer->setOutputFormat(QPrinter::PdfFormat);

    QString fileName = QFileDialog::getSaveFileName((QWidget* )0, "Export PDF", QString(), "*.pdf");
    if (QFileInfo(fileName).suffix().isEmpty()) { fileName.append(".pdf"); }

    printer->setOutputFileName(fileName);


        QPainter previewPaint(printer);

        previewPaint.setRenderHints(QPainter::Antialiasing |
                               QPainter::TextAntialiasing |
                               QPainter::SmoothPixmapTransform, true);

        for (int i = 0; i < loadedImages.size(); ++i)
                {
                    QImage img(loadedImages[i].loadedImage::getFileName());
                    QRectF printerRect = printer->pageRect(QPrinter::DevicePixel);
                    QSizeF printerSizeF = printerRect.size();
                    QSize printerSize = printerSizeF.toSize();
                    QImage img2;
                    QImage img3;
                    QTransform tr;
                    tr.rotate(loadedImages[i].loadedImage::getRotation());
                    img3 = img.transformed(tr);
                    if (loadedImages[i].loadedImage::getFitPage() == 1)
                    {
                        img2 = img3.scaled(printerSize, Qt::KeepAspectRatio, Qt::SmoothTransformation);
                    }
                    else {
                        img2 = img3;
                    }

                    qDebug() << loadedImages[i].loadedImage::getFileName();

                    previewPaint.drawImage(QPoint(0,0),img2);
                    if (i != fileNames.size() - 1)
                    {
                        printer->newPage();
                    }
         }
}

void MainWindow::rotate()
{
    if (loadedImages.size() != 0)
    {
        int aktualni_strana = previewWidget->currentPage();
        loadedImages[aktualni_strana - 1].loadedImage::rotateRight();
        previewWidget->updatePreview();
    }


}

void MainWindow::rotate_left()
{
    if (loadedImages.size() != 0)
    {
        int aktualni_strana = previewWidget->currentPage();
        loadedImages[aktualni_strana - 1].loadedImage::rotateLeft();
        previewWidget->updatePreview();
    }


}

void MainWindow::image_fit()
{
    if (loadedImages.size() != 0)
    {
        if (loadedImages[0].loadedImage::getFitPage() == 0)
        {
            for (int i = 0; i < loadedImages.size(); i++)
            {
                loadedImages[i].loadedImage::setFitPage(1);
                //qDebug() << "Strana: " << i << "   iFit: " << loadedImages[i].loadedImage::getFitPage();
            }
        }
        else if (loadedImages[0].loadedImage::getFitPage() == 1)
        {
            for (int i = 0; i < loadedImages.size(); i++)
            {
                loadedImages[i].loadedImage::setFitPage(0);
                //qDebug() << "Strana: " << i << "   iFit: " << loadedImages[i].loadedImage::getFitPage();
            }
        }
        previewWidget->updatePreview();
    }
}

void MainWindow::moveBackSlot(){
    int aktualni_strana = previewWidget->currentPage();
    int idx = aktualni_strana - 1;
    if (idx != 0)
    {
        loadedImages.swapItemsAt(idx - 1, idx);
    }
    previewWidget->setCurrentPage(aktualni_strana - 1);
    previewWidget->updatePreview();

}

void MainWindow::moveForwardSlot(){
    int aktualni_strana = previewWidget->currentPage();
    int idx = aktualni_strana - 1;
    if (loadedImages.size() != 0)
    {
        if (idx != loadedImages.size() - 1)
        {
            loadedImages.swapItemsAt(idx, idx + 1);
        }
        previewWidget->setCurrentPage(aktualni_strana + 1);
        previewWidget->updatePreview();
    }
}

void MainWindow::first_page(){
    previewWidget->setCurrentPage(1);
}

void MainWindow::last_page(){
    previewWidget->setCurrentPage(loadedImages.size());
}

void MainWindow::prev_page(){
    int aktualni_strana = previewWidget->currentPage();
    if (aktualni_strana != 1)
    {
        previewWidget->setCurrentPage(aktualni_strana - 1);
    }
}

void MainWindow::next_page(){
    int aktualni_strana = previewWidget->currentPage();
    if (aktualni_strana != loadedImages.size())
    {
        previewWidget->setCurrentPage(aktualni_strana + 1);
    }
}

void MainWindow::zoom_out(){
    previewWidget->zoomOut();
}

void MainWindow::zoom_in(){
    previewWidget->zoomIn();
}

void MainWindow::landscape_slot(){
    previewWidget->setLandscapeOrientation();
}

void MainWindow::portrait_slot(){
    previewWidget->setPortraitOrientation();
}

void MainWindow::page_setup_slot(){
    QPageSetupDialog pageDialog (printer);
    //previewWidget->setOrientation();
    if (pageDialog.exec() != QDialog::Accepted)
    {
        return;
    }
    previewWidget->updatePreview();
    //pageDialog.open()
}

void MainWindow::single_page()
{
    previewWidget->setViewMode(QPrintPreviewWidget::SinglePageView);
}

void MainWindow::facing_page()
{
    previewWidget->setViewMode(QPrintPreviewWidget::FacingPagesView);
}

void MainWindow::all_page()
{
    previewWidget->setViewMode(QPrintPreviewWidget::AllPagesView);
}

void MainWindow::fit_page_view()
{
    previewWidget->setZoomMode(QPrintPreviewWidget::FitInView);
}

void MainWindow::fit_width_view()
{
    previewWidget->setZoomMode(QPrintPreviewWidget::FitToWidth);
}

void MainWindow::print(QPrinter * printer)
{
    QPainter previewPaint(printer);

    previewPaint.setRenderHints(QPainter::Antialiasing |
                           QPainter::TextAntialiasing |
                           QPainter::SmoothPixmapTransform, true);

    for (int i = 0; i < loadedImages.size(); ++i)
            {
                QImage img(loadedImages[i].loadedImage::getFileName());
                QRectF printerRect = printer->pageRect(QPrinter::DevicePixel);
                QSizeF printerSizeF = printerRect.size();
                QSize printerSize = printerSizeF.toSize();
                QImage img2;
                QImage img3;
                QTransform tr;
                tr.rotate(loadedImages[i].loadedImage::getRotation());
                img3 = img.transformed(tr);
                if (loadedImages[i].loadedImage::getFitPage() == 1)
                {
                    img2 = img3.scaled(printerSize, Qt::KeepAspectRatio, Qt::SmoothTransformation);
                }
                else {
                    img2 = img3;
                }

                //qDebug() << loadedImages[i].loadedImage::getFileName();

                previewPaint.drawImage(QPoint(0,0),img2);
                if (i != fileNames.size() - 1)
                {
                    printer->newPage();
                }
            }
}
