QT       += core gui
QT += printsupport
QT += widgets
QT += gui




greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    mainwindow.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    mainwindow.qrc

DISTFILES += \
    icons/all_page.png \
    icons/facing_page.png \
    icons/first_page.png \
    icons/fit_page.png \
    icons/fit_width.png \
    icons/image_fit.png \
    icons/landscape.png \
    icons/last_page.png \
    icons/move_down.png \
    icons/move_up.png \
    icons/next_page.png \
    icons/open-folder.png \
    icons/page_dialog.png \
    icons/portrait.png \
    icons/prev_page.png \
    icons/printer.png \
    icons/rotation_left.png \
    icons/rotation_right.png \
    icons/single_page.png \
    icons/zoom_in.png \
    icons/zoom_out.png
