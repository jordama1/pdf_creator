#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QPrinter>
#include <QPrintPreviewWidget>

class QScrollArea;
class QListWidget;
//class QPrinter;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class loadedImage
{
private:
    int rotation;
    QString fileName;
    int fitPage;
public:
    loadedImage(QString file_name);
    //~loadedImage();
    void rotateRight();
    void rotateLeft();
    QString getFileName();
    void setFileName (QString file_name);
    int getRotation();
    void setFitPage(int fit_page);
    int getFitPage();
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


private slots:
    void on_actionOpen_triggered();

    void on_actionExit_triggered();

    void on_actionPrint_triggered();

    void print(QPrinter *);

    void moveBackSlot();

    void moveForwardSlot();

    void rotate();

    void rotate_left();

    void image_fit();

    void first_page();

    void last_page();

    void next_page();

    void prev_page();

    void zoom_out();

    void zoom_in();

    void landscape_slot();

    void portrait_slot();

    void page_setup_slot();

    void single_page();

    void facing_page();

    void all_page();

    void fit_width_view();

    void fit_page_view();


private:
    Ui::MainWindow *ui;

    QStringList fileNames;
    QPrinter * printer;

    QVector <loadedImage> loadedImages;

    QPrintPreviewWidget * previewWidget = new QPrintPreviewWidget(printer);

};
#endif // MAINWINDOW_H
