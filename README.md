# pdf_creator

PDF Creator

Tento projekt je program vytvořený v C++ a ve frameworku QT umožňující vytváření PDF dokumentů z nahraných obrázků. Hlavní okno programu je tvořeno toolbarem a hlavním zobrazovacím widgetem (QPrintpreviewWidget), který zobrazuje náhled tisku.
Projekt byl kompilován pomocí defaultní metody Qt Creatoru (qmake). V adresáři build-PDF_maker-Desktop_Qt_6_1_0_MinGW_64_bit-Release\release je spustitelný program PDF_maker, spolu se všemi dynamicky linkovanými knihovnami nutnými ke spuštění.
Ikony na toolbaru umožňují po rozkliknutí načíst obrázky pomocí dialogu, otáčet obrázky a měnit jejich pořadí, měnit orientaci stránek, měnit zoom a režim náhledu tisku, upravit velikost obrázku na velikost stránky a listovat mezi stránkami, otevřít dialog na nastavení formátu tisku a okrajů. Na hlavním widgetu se zobrazuje náhled tisku, který se aktualizuje s každou provedenou změnou. Po kliknutí na tlačítko tisk se otevře dialog pro vytištění daného dokumentu do PDF.

